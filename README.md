# my dotfiles

Yuto Tanaka's dotfiles.
managed by [chezmoi](https://www.chezmoi.io/).

## Setup

### Linux, macOS

```shell
curl -L https://dot.mimikun.dev | sh
```

### Windows

See: [windows/README.md](windows/README.md)

