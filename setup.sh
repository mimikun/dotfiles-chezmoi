mkdir -p ~/.config/nvim/colors
wget https://raw.githubusercontent.com/sickill/vim-monokai/master/colors/monokai.vim -O ~/.config/nvim/colors/monokai.vim

mkdir -p ~/.vim/colors/
wget https://raw.githubusercontent.com/sickill/vim-monokai/master/colors/monokai.vim -O ~/.vim/colors/monokai.vim
